from django.db import models

# Create your models here.

class URL(models.Model):
    actual_url = models.URLField(max_length=400, unique=True)
    short_url = models.URLField(max_length=100, unique=True)
    hits = models.IntegerField(default=0)
                                                                                
    def __str__(self):
        return f"URL:{str(self.short_url)}  ||  Hits: {str(self.hits)} ||\
         RedirectTo:{str(self.actual_url)}"

    class Meta:
        constraints=[
            models.UniqueConstraint(
                fields=['actual_url'],
                name="actual_url_uniqueness"
            ),
            models.UniqueConstraint(
                fields=['short_url'],
                name="short_url_uniqueness"
            )
        ]