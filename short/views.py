from django.shortcuts import render, redirect
from django.views import View
from django.db.models import F

from .models import URL
from .forms import ShortenerForm

import hashlib
import base64

# Create your views here.
class ShortenerView(View):
    def post(self, request):
        input_url = request.POST.get('actual_url')
        frm = ShortenerForm(request.POST)
        if frm.is_valid():
            data = frm.cleaned_data
            actual_url = data.get("actual_url")
            try:
                hash = hashlib.md5(actual_url.encode('utf-8')).digest()
                hash = hash[:7]
                shorten_ext = str(base64.b64encode(hash),'utf-8')
                short_url = 'http://'+request.get_host() + '/' + shorten_ext
                url = URL(
                    actual_url=actual_url,
                    short_url = shorten_ext
                )
                url.save()
            except Exception as e:
                return render(request, 'input.html', {'msg':str(e)})
            user_data = {
                'shortened_url':short_url,
                'hits':url.hits
            }
            return render(request, 'input.html', {'data':user_data})
        else:
            return render(request, 'input.html',{"form":ShortenerForm(), 'msg'\
            :frm.errors})

    def get(self, request):
        frm=ShortenerForm()
        return render(request, 'input.html', {"form":frm})

class HitActualView(View):
    def get(self, request,**kwargs):
        tiny_url = kwargs.get("tiny_name")
        try:
            act = URL.objects.get(short_url=tiny_url)
            actual_url = act.actual_url
            act.hits += 1
            act.save()
        except Exception as e:
            return render(request, 'input.html', {'msg':str(e)})
        return redirect(actual_url)