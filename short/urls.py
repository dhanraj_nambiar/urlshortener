from django.urls import path

from .views import *

urlpatterns = [
    path('shorten/',ShortenerView.as_view(), name="shortener"),
    path('<str:tiny_name>', HitActualView.as_view())
]