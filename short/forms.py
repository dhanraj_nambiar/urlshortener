from django.forms import ModelForm

from .models import URL

class ShortenerForm(ModelForm):
    class Meta:
        model = URL
        fields = ['actual_url']