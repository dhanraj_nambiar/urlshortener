################################App setup start##################################
mkdir <directoryname>
cd <directoryname>
git clone https://gitlab.com/dhanraj_nambiar/urlshortener.git
cd urlshortener
virtualenv <environname>
source <environname>/bin/actiavte
pip install -r requirements.txt

Set up MySQL database as given in section below. Once done setup continue as here 
below.

python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic
create a superuser for admin login 'python3 manage.py createsuperuser'

################################App setup end##################################
################################ MySQL start #################################
This part mentions steps of installing MySQL 
sudo apt-get update
sudo apt-get install mysql
sudo apt install libmysqlclient-dev
sudo systemctl enable mysql

Creating a DB user for application and granting it all privileges for specific database
mysql
>
CREATE USER 'fingent' IDENTIFIED WITH mysql_native_password BY 'Fifaworldcup@2022';
>
CREATE DATABASE custom_fingent CHARACTER SET utf8;
>
GRANT ALL PRIVILEGES ON custom_fingent.* TO 'fingent';

Change DB Backend to MySQL in settings file. Given in DATABSE variable of settings.
################################ MySQL end #################################


################################ Apache & Modwsgi 1 start ##########################
This approach 1 is traditional way. It's relatively harder for configuring for a newbie to Apache.

sudo apt install apache2
download mod_wsgi from https://github.com/GrahamDumpleton/mod_wsgi/releases
./configure
make
edit the file /etc/apache2/sites-available/000-default.conf & add
<VirtualHost *.80>
        <Directory /home/sinergia/Learning/interview_tasks/fingent_tasks/URLshort/URLshort/wsgi.py>
                <Files wsgi.py>
                        Require all granted
                </Files>
        </Directory>
        WSGIDaemonProcess fingent_proj python-home=/home/sinergia/Learning/interview_tasks/fingent_tasks/env python-path=/home/sinergia/Learning/interview_tasks/fingent_tasks/URLshort
        WSGIProcessGroup fingent_proj
        WSGIScriptAlias / /home/sinergia/Learning/interview_tasks/fingent_tasks/URLshort/URLshort/wsgi.py
        #WSGIPythonHome /home/sinergia/Learning/interview_tasks/fingent_tasks/env/
        #WSGIPythonPath /home/sinergia/Learning/interview_tasks/fingent_tasks/URLshort
</VirtualHost>

sudo systemctl restart apache2.service
################################ Apache & Modwsgi 1 end ##########################

################################ Apache & Modwsgi 2 start ##########################
This second approach is simpler which generates all the Apache configuration by default,
equivalent to approach given above. Approach1 could be better for more granular control
over the configuration.

sudo apt install apache2
pip install mod-wsgi==4.9.4
add  'mod_wsgi.server' to INSTALLED_APPS in settings

The above pip install creates a executable  mod_wsgi-express in ~/.local/bin, which is needed to run the modwsgi. To include the folder in system PATH variable do as below.

vim ~/.bashrc
add '''export PATH="~/.local/bin:$PATH"''' to last line & save.
reboot to make changes affected(Apache's as well as PATH variable).
################################ Apache & Modwsgi 2 end ##########################

To run the application using mod_wsgi. Go to project base directory and run 
"python3 manage.py runmodwsgi".
To view all shortened url's go to admin page http://127.0.0.1:8000/admin/ and login
using superuser credentials created above.